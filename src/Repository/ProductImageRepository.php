<?php

namespace App\Repository;

use App\Entity\ProductImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductImage[]    findAll()
 * @method ProductImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductImageRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(ManagerRegistry $registry, private EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, ProductImage::class);
    }

    /**
     * @param int $page
     * @param int $perPage
     * @return array
     */
    public function get(int $page = 0, int $perPage = 10)
    {

        $currentPage = max(1, $page);
        $offset = ($currentPage - 1) * $perPage;

        $dql = 'SELECT pi FROM App\Entity\ProductImage as pi JOIN App\Entity\Product as p';
        $query = $this->entityManager->createQuery($dql)
            ->setFirstResult($offset)
            ->setMaxResults($perPage);

        $paginator = new Paginator($query, true);
        $result = [];

        foreach ($paginator as $productImage) {
            $products = [];
            if ($productImage instanceof ProductImage && !$productImage->getProducts()->isEmpty()) {
                foreach ($productImage->getProducts() as $product) {
                    $products[] = [
                        'id' => $product->getId(),
                        'title' => $product->getTitle(),
                        'createdAt' => $product->getCreatedAt(),
                    ];
                }
            }
            $result[] = [
                'id' => $productImage->getId(),
                'title' => $productImage->getTitle(),
                'createdAt' => $productImage->getCreatedAt(),
                'products' => $products
            ];
        }

        return $result;
    }

}
