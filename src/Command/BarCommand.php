<?php
/**
 * Created by
 * Andrey D. Gluschenko
 * AccessDenied80@gmail.com
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BarCommand extends Command implements ChainInterface
{
    use ChainTrait;

    protected static $defaultName = 'bar:hi';

    protected function configure()
    {
        $this->setDescription('This is the bar:hi command who depends of foo:hello');
        $this->setMasterCommand(FooCommand::class);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->logger->debug('[' . date('Y-m-d H:i:s') . '] Hi from Bar!');

        $output->writeln('Hi from Bar!');
        return Command::SUCCESS;
    }
}