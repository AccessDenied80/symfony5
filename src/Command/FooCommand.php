<?php
/**
 * Created by
 * Andrey D. Gluschenko
 * AccessDenied80@gmail.com
 */

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FooCommand extends Command
{
    protected static $defaultName = 'foo:hello';

    public function __construct(private LoggerInterface $logger)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output, ): int
    {
        $this->logger->debug('[' . date('Y-m-d H:i:s') . '] Hello from Foo!');

        $output->writeln('Hello from Foo!');
        return Command::SUCCESS;
    }
}