<?php
/**
 * Created by
 * Andrey D. Gluschenko
 * AccessDenied80@gmail.com
 */

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Event\ConsoleEvent;

class Listener
{

    public function __construct(private LoggerInterface $logger)
    {
    }

    public function onConsoleCommand(ConsoleEvent $event)
    {
        $command = $event->getCommand();

        if ($command instanceof ChainInterface) {
            $name = $command->getName();
            $event->stopPropagation();
            throw new \Exception(
                $name . ' command is a member of a command chain and cannot be executed on its own'
            );
        }
    }


    public function onConsoleTerminate(ConsoleEvent $event)
    {
        $command = $event->getCommand();

        array_map(function ($command) use ($event) {
            $this->logger->debug('[' . date('Y-m-d H:i:s') . '] ' . 'Executing ' . $command->getName() . ' chain members:');

            $command->run($event->getInput(), $event->getOutput());
        }, array_filter($command->getApplication()->all(), function ($child) use ($command) {
            if (($child instanceof ChainInterface) && get_class($command) === $child->getMaster()) {

                $this->logger->debug('[' . date('Y-m-d H:i:s') . '] ' . 'Executing ' . $command->getName() . ' command itself first:');

                return $command;
            }
        }));
    }
}