<?php
/**
 * Created by
 * Andrey D. Gluschenko
 * AccessDenied80@gmail.com
 */

namespace App\Command;

use Psr\Log\LoggerInterface;

trait ChainTrait
{
    /**
     * @var array
     */
    protected $commands = [];

    public function __construct(private LoggerInterface $logger)
    {
        parent::__construct();
    }

    private function getCommandName($class)
    {
        $r = new \ReflectionClass($class);
        return $r->getStaticPropertyValue('defaultName');
    }

    /**
     * @param $command
     */
    public function setMasterCommand($masterCommand): void
    {
        array_push($this->commands, $masterCommand);

        $masterCommandName = $this->getCommandName($masterCommand);
        $this->logger->debug('[' . date('Y-m-d H:i:s') . '] ' . $masterCommandName . ' is a master command of a command chain that has registered member commands');
        $this->logger->debug('[' . date('Y-m-d H:i:s') . '] ' . $this->getName() . ' registered as a member of ' . $masterCommandName . ' command chain');
    }

    /**
     * @return array
     */
    public function getMasters(): array
    {
        return $this->commands;
    }

    /**
     * @return string
     */
    public function getMaster(): string
    {
        return $this->commands[0] ?? '';
    }

}