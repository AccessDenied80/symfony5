<?php
/**
 * Created by
 * Andrey D. Gluschenko
 * AccessDenied80@gmail.com
 */

namespace App\Command;

interface ChainInterface
{
    public function setMasterCommand($masterCommand): void;

    public function getMasters(): array;

    public function getMaster(): string;

}