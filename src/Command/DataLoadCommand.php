<?php

namespace App\Command;

use App\Entity\Product;
use App\Entity\ProductImage;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DataLoadCommand extends Command
{
    protected static $defaultName = 'data:load';
    protected static $defaultDescription = 'Generate products and images to db';

    public function __construct(private EntityManagerInterface $entityManager)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $faker = Factory::create();

            for ($ip = 0; $ip < 100; $ip++) {
                $productImage = new ProductImage();
                $productImage->setTitle('Image - ' . $faker->unique()->realText(50));

                $this->entityManager->persist($productImage);

                $productImages[] = $productImage;
            }

            for ($p = 0; $p < 500; $p++) {
                $product = new Product();
                $product->setTitle('Product - ' . $faker->unique()->realText(50));
                $product->setImage($faker->randomElement($productImages));

                $this->entityManager->persist($product);
            }

            $this->entityManager->flush();

            $io->success('Success');

            return Command::SUCCESS;

        } catch (\Throwable $e) {

            $io->error($e->getMessage());
            return Command::FAILURE;

        }
    }
}
