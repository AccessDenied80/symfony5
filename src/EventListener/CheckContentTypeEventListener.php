<?php
namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

/**
 *
 */
class CheckContentTypeEventListener
{
    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        if ($event->getRequest()->headers->get('Content-type') !== 'application/json') {
            throw new UnsupportedMediaTypeHttpException();
        }
    }
}
