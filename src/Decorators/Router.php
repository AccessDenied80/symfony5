<?php

namespace App\Decorators;

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RouterInterface;

/**
 *
 */
class Router implements RouterInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param RequestContext $context
     */
    public function setContext(RequestContext $context)
    {
        $this->router->setContext($context);
    }

    /**
     * @return RequestContext
     */
    public function getContext()
    {
        return $this->router->getContext();
    }

    /**
     * @return RouteCollection
     */
    public function getRouteCollection()
    {
        return $this->router->getRouteCollection();
    }

    /**
     * @param string $name
     * @param array $parameters
     * @param int $referenceType
     * @return string
     */
    public function generate(string $name, array $parameters = [], int $referenceType = self::ABSOLUTE_PATH)
    {
        return urldecode($this->router->generate($name, $parameters, $referenceType));
    }

    /**
     * @param string $pathinfo
     * @return array
     */
    public function match(string $pathinfo)
    {
        return $this->router->match($pathinfo);
    }
}