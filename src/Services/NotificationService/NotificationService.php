<?php
/**
 * Created by
 * Andrey D. Gluschenko
 * AccessDenied80@gmail.com
 */

namespace App\Services\NotificationService;

use Psr\Container\ContainerInterface;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

/**
 *
 */
class NotificationService
{

    /**
     * @param ContainerInterface $serviceLocator
     */
    public function __construct(private ContainerInterface $serviceLocator)
    {

    }

    /**
     * @param string $channel
     * @param string $message
     * @param string $recipient
     */
    public function send(string $channel, string $message, string $recipient)
    {

        if (!$this->serviceLocator->has($channel)) {
            throw new InvalidArgumentException('Invalid channel');
        }

        $messageDto = new Message();
        $messageDto->setRecipient($recipient);
        $messageDto->setMessage($message);

        $this->serviceLocator->get($channel)
            ->send($messageDto);

    }


}