<?php

namespace App\Services\NotificationService;

use Symfony\Component\Notifier\ChatterInterface;
use Symfony\Component\Notifier\Message\ChatMessage;

/**
 *
 */
class TelegramNotificationChannelService implements NotificationChannelServiceInterface
{

    private $notifier;

    /**
     * @param $notifier
     */
    public function __construct(ChatterInterface $notifier)
    {
        $this->notifier = $notifier;
    }

    public function send(Message $message)
    {
        $chatMessage = (new ChatMessage($message->getMessage()))
            ->transport('telegram');

        $this->notifier->send($chatMessage);
    }
}