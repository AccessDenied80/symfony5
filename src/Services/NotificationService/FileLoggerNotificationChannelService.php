<?php

namespace App\Services\NotificationService;

use Psr\Log\LoggerInterface;

/**
 *
 */
class FileLoggerNotificationChannelService implements NotificationChannelServiceInterface
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Message $message
     */
    public function send(Message $message)
    {
        $this->logger->info($message->getMessage());
    }
}