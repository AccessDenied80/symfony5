<?php

namespace App\Services\NotificationService;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 *
 */
class EmailNotificationChannelService implements NotificationChannelServiceInterface
{
    /**
     * @var MailerInterface
     */
    private MailerInterface $mailer;

    /**
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param Message $message
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function send(Message $message)
    {
        $email = (new Email())
            ->from('symfony@dev')
            ->to($message->getRecipient())
            ->subject('Time for Symfony Mailer!')
            ->text($message->getMessage())
            ->html('<p>' . $message->getMessage() . '</p>');

        $this->mailer->send($email);
    }
}