<?php
namespace App\Services\NotificationService;

interface NotificationChannelServiceInterface
{
    public function send(Message $message);
}