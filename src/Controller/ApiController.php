<?php

namespace App\Controller;


use App\Repository\ProductImageRepository;
use App\Repository\ProductRepository;
use App\Services\NotificationService\NotificationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 *
 */
class ApiController extends AbstractController
{
    /**
     * @param Request $request
     * @return Response
     */
    #[Route(
        '/notify',
        name: 'api.notify',
        methods: 'POST',
        format: 'json'
    )]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function notify(Request $request, NotificationService $notificationService): Response
    {
        try {
            $channel = $request->request->get('channel');
            $message = $request->request->get('message');
            $recipient = $request->request->get('recipient');

            $notificationService->send($channel, $message, $recipient);

            return $this->json(['Notification send success']);

        } catch (\Throwable $e) {
            return $this->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    #[Route(
        '/',
        name: 'api',
        requirements: [
            '_format' => 'json'
        ],
        methods: 'GET',
        format: 'json'
    )]
    public function index(): Response
    {
        $products = $this->readCSV();
        $productsUrl = [];

        foreach ($products as $product) {
            $productsUrl[] = $this->generateUrl('api.product', ['slug' => $product['sku']], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        $urls = [
            $this->generateUrl('api', [], UrlGeneratorInterface::ABSOLUTE_URL),
            $this->generateUrl('api.my_info', [], UrlGeneratorInterface::ABSOLUTE_URL)
        ];

        return $this->json(array_merge($urls, $productsUrl));
    }

    /**
     * @param Request $request
     * @return Response
     */
    #[Route(
        '/my-info',
        name: 'api.my_info',
        methods: 'GET',
        format: 'json'
    )]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function myInfo(Request $request): Response
    {
        return $this->json([
            'ip' => $request->getClientIp(),
            'language' => $request->getDefaultLocale(),
            'browser' => $request->headers->get('User-Agent'),
        ]);
    }

    #[Route(
        '/product',
        name: 'api.product_create',
        methods: 'POST',
        format: 'json'
    )]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function productCreate(Request $request, ProductRepository $productRepository): Response
    {
        $title = $request->get('title');
        $result = $productRepository->create($title);
        return $this->json($result);
    }

    #[Route(
        '/product/image',
        name: 'api.product.image',
        methods: 'GET',
        format: 'json'
    )]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function productImage(Request $request, ProductImageRepository $productImageRepository): Response
    {
        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 10);

        $productImageResult = $productImageRepository->get($page, $perPage);
        return $this->json($productImageResult);
    }

    /**
     * @param TranslatorInterface $translator
     * @param string $slug
     * @return Response
     */
    #[Route(
        '/{slug}',
        name: 'api.product',
        requirements: ['slug' => '.+'],
        methods: 'GET',
        format: 'json'
    )]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function product(TranslatorInterface $translator, string $slug): Response
    {
        $products = $this->readCSV();

        foreach ($products as $product) {
            if ($product['sku'] === $slug) {
                return $this->json([
                    $translator->trans('api.product_name') => $product['name'],
                    $translator->trans('api.product_description') => $product['description'],
                    $translator->trans('api.product_sku') => $product['sku']
                ]);
            }
        }

        throw new NotFoundHttpException();
    }

    /**
     * @return array
     */
    private function readCSV(): array
    {
        $fromFile = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR . 'products.csv';

        if ($fromFile == false || !file_exists($fromFile)) {
            throw new NotFoundResourceException('Csv file not found');
        }

        $file = fopen($fromFile, 'r');

        $products = [];

        foreach ($this->getRowsIterator($file) as $key => $row) {
            $this->line = $key;
            if ($key > 0 && isset($row[0])) {
                $products[] = [
                    'sku' => $row[0],
                    'name' => $row[1],
                    'description' => $row[2]
                ];
            }
        }

        fclose($file);

        return $products;
    }

    /**
     * @return Generator|void
     */
    protected function getRowsIterator($file)
    {
        while (!feof($file)) {
            $row = fgetcsv($file);
            yield $row;
        }
    }
}
